#! /bin/bash

OSVer=`../MakeSname`
BinDir=../bin/$OSVer

mkdir $HOME/bin
cp $BinDir/mdtmApp $HOME/bin
echo "export PATH=$PATH:$HOME/bin" >> $HOME/.bashrc
