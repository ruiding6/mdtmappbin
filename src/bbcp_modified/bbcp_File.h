#ifndef __BBCP_FILE_H__
#define __BBCP_FILE_H__
/******************************************************************************/
/*                                                                            */
/*                           b b c p _ F i l e . h                            */
/*                                                                            */
/* (c) 2002 by the Board of Trustees of the Leland Stanford, Jr., University  */
/*      All Rights Reserved. See bbcp_Version.C for complete License Terms    *//*                            All Rights Reserved                             */
/*   Produced by Andrew Hanushevsky for Stanford University under contract    */
/*              DE-AC03-76-SFO0515 with the Department of Energy              */
/******************************************************************************/

#include <string.h>
#include "bbcp_BuffPool.h"
#include "bbcp_IO.h"
#include "bbcp_Pthread.h"
#include "bbcp_Timer.h"
#include "MDTMApp_TaskGroup.h"

// The bbcp_File class describes the set of operations that are needed to copy
// a "file". The actual I/O are determined by the associated filesystem and
// are specified during instantiation.

class  bbcp_FileChkSum;
class  bbcp_FileSystem;

class bbcp_File
{
public:

// Return target file system
//
bbcp_FileSystem *Fsys() {return FSp;}

// Return FD number
//
int          ioFD() {return IOB->FD();}

// Read a record from a file
//
ssize_t      Get(char *buff, size_t blen) {return IOB->Read(buff, blen);}

// Internal buffer-to-buffer passthrough function
//
int          Passthru(bbcp_BuffPool *iBP, bbcp_BuffPool *oBP,
                      bbcp_FileChkSum *csP, int nstrms);

// Return path to the file
//
char        *Path() {return iofn;}

// Write a record to a file
//
ssize_t      Put(char *buff, size_t blen) {return IOB->Write(buff, blen);}

// Read_All() reads the file until eof and returns 0 (success) or -errno.
//
int          Read_All(bbcp_BuffPool *buffpool, int Bfact);

// Write_All() writes the file until eof and return 0 (success) or -errno.
//
int          Write_All(bbcp_BuffPool *buffpool, int nstrms);

// Sets the file pointer to read or write from an offset
//
int          Seek(long long offv) {nextoffset = offv; return IOB->Seek(offv);}

// setSize() sets the expected file size
//
void         setSize(long long top) {lastoff = top;}
void         setTid(pthread_t worker) {tid = worker;}
pthread_t    getTid() {return tid;}
void         setLoad(long long load) {bytesLeft = load; tload = load;}
// Stats() reports the i/o time and buffer wait time in milliseconds and
//         returns the total number of bytes transfered.
//
long long    Stats(double &iotime) {return IOB->ioStats(iotime);}
long long    getOffset() {return nextoffset;}
void         setBuddy(bbcp_File *mybuddy) {Buddy = mybuddy;}
static void  setNudge() {Nudge = 1; Wait = 1;}
long long    Stats()               {if(aio_mode) return DoneBytes;
                                    else return IOB->ioStats();}
bool         UpdateStat(int len);
             bbcp_File(const char *path, bbcp_IO *iox,
                       bbcp_FileSystem *fsp, int secSize=0);

            ~bbcp_File() {if (iofn) {free(iofn); iofn = 0;}
                          if (IOB)  {delete IOB; IOB  = 0;}
                         }

int          seqno;
int          bufreorders;
int          maxreorders;

bbcp_FileSystem *FSp;
bbcp_IO         *IOB;
char            *iofn;
bbcp_BuffPool   *bbcp_BPool;
MDTMApp_Task    *Tobj;
long long        blockSize;
long long        tload;
bbcp_File       *next;

private:
static int      Nudge;
static int      Wait;
bbcp_Semaphore  Rendezvous;
   
bbcp_Buffer    *nextbuff;
long long       nextoffset;
long long       DoneBytes;
long long       bytesLeft;
long long       PaceTime;
long long       lastoff;
bbcp_Timer      Ticker;
int             rtCopy;
int             curq;
bool            aio_mode;
pthread_t       tid;
bbcp_File       *Buddy;
bbcp_Mutex      OffsetLock;

bbcp_Buffer *getBuffer(long long offset);
int          Read_Direct (bbcp_BuffPool *inB, bbcp_BuffPool *otP);
int          Read_Pipe   (bbcp_BuffPool *inB, bbcp_BuffPool *otP);
int          Read_Normal (bbcp_BuffPool *inB, bbcp_BuffPool *otP);
int          Read_Vector (bbcp_BuffPool *inB, bbcp_BuffPool *otP, int vN);
int          Read_Aio    (bbcp_BuffPool *iBP);
void         Read_Wait   (int rdsz);
void         Read_Wait   ();
int          verChkSum(bbcp_FileChkSum *csP);
int          Write_Direct(bbcp_BuffPool *iBP, bbcp_BuffPool *oBP, int nstrms);
int          Write_Normal(bbcp_BuffPool *iBP, bbcp_BuffPool *oBP, int nstrms);
};
#endif
