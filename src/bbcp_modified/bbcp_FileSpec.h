#ifndef __BBCP_FILESPEC_H__
#define __BBCP_FILESPEC_H__
/******************************************************************************/
/*                                                                            */
/*                       b b c p _ F i l e S p e c . h                        */
/*                                                                            */
/* (c) 2002 by the Board of Trustees of the Leland Stanford, Jr., University  */
/*      All Rights Reserved. See bbcp_Version.C for complete License Terms    */
/*                            All Rights Reserved                             */
/*   Produced by Andrew Hanushevsky for Stanford University under contract    */
/*              DE-AC03-76-SFO0515 with the Department of Energy              */
/******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <utime.h>
#include <assert.h>
#include "bbcp_FileSystem.h"
#include <iostream>

using namespace std;  
// Parse a spec formed as: [username@][hostname:]fname
//
class bbcp_FileSpec
{
public:

bbcp_FileSpec        *next;//pointer to the next element
char                 *username;
char                 *hostname;
char                 *pathname;
char                 *filename;
char                 *filereqn;
char                 *fileargs;
char                 *targpath;
char                 *targetfn;
char                 *symbname;
char                 *SID;
char                 *cksum;
long long             targetsz;
char                 *targsigf;
int                   seqno;//sequence number of this file
struct bbcp_FileInfo  Info;
int		      devID;
long long             phyoff;
long long             Options;
bbcp_FileSystem      *FSp;
bbcp_File            *fop;
bool                  OpenFlag;

int              Compose(long long did, char *dpath, int dplen, char *fname);

int              Create_Path();

int              Decode(char *buff, char *xName=0);

int              Encode(char *buff, size_t blen);

void             ExtendFileSpec(bbcp_FileSpec* headp, int &numF);

int              Finalize(int retc=0, int mode=-1);

bbcp_FileSystem *FSys() {return FSp;}

void             Parse(char *spec, int isPipe=0);

int              setMode(mode_t Mode);

int              setStat();

int              GetFS(char *Path);

int              Stat(int complain=1);

int              WriteSigFile();

int              Xfr_Done();

     bbcp_FileSpec(bbcp_FileSystem *fsp=0, char *hname = 0, char *uname=0)
                  : next(0), username(uname), hostname(hname), pathname(0),
                    filename(0), filereqn(0), fileargs(0), SID(0), fop(0),
                    targpath(0), targetfn(0), targetsz(0), targsigf(0),
                    fspec(0), fspec1(0), fspec2(0), FSp(fsp), phyoff(0),
                    Options(0), cksum(0), OpenFlag(false), seqno(0), 
                    devID(0), symbname(0)
                    {memset(&Info, 0, sizeof(struct bbcp_FileInfo));}
    ~bbcp_FileSpec() {if (fspec)    free(fspec);
                      if (fspec1)   free(fspec1);
                      if (fspec2)   free(fspec2);
                      if (targpath) free(targpath);
                      if (targsigf) free(targsigf);
                      if (FSp)  {delete FSp; FSp = 0;}
                      if (SID)  {free(SID); SID = 0;}
                      if (cksum)  {free(cksum); cksum = 0;}
                      if (filename) {free(filename); filename = 0;}
                      if (pathname) {free(pathname); pathname = 0;}
                      if (symbname) {free(symbname); symbname = 0;}
                     }

private:
char            *fspec;
char            *fspec1;
char            *fspec2;
int              Xfr_Fixup();
void             BuildPaths();
};
#endif
