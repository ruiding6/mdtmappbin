#define _BSD_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <search.h>

#define SIZE 4096

char key_mat[SIZE][SIZE];
char dev_mat[SIZE][SIZE];
int key_cnt;

int putHtable(char *start, char *end){
  char *cp = start, *nstart, *nend, *smajor, *sminor, *dev_name;
  int major, minor;
  ENTRY entry;
  dev_t dev_id;
  entry.key = key_mat[key_cnt];
  
  printf("Get entry: %s\n", start);

//Get major number 
  while(*cp == ' ') cp++;
  nstart = cp; cp++;
  while(*cp != ' ' && *cp != '\0') cp++;
  if(*cp == '\0') return 0;
  nend = cp;
  if((smajor = strndup(nstart, nend - nstart)) == NULL) return 0;
  printf("smajor = %s\n", smajor);
  major = atoi(smajor); 

//Get minor number
  while(*cp == ' ') cp++;
  nstart = cp; cp++;
  while(*cp != ' ' && *cp != '\0') cp++;
  if(*cp == '\0') return 0;
  nend = cp;
  if((sminor = strndup(nstart, nend - nstart)) == NULL) return 0;
  minor = atoi(sminor);

//Get device name
  while(*cp == ' ') cp++;
  cp++;
  while(*cp != ' ' && *cp != '\0') cp++;
  cp++;
  while(*cp == ' ') cp++;
  nstart = cp; cp++;
  while(*cp != ' ' && *cp != '\0') cp++;
  if(*cp != '\0') return 0;
  nend = cp;
  if((strncpy(dev_mat[key_cnt], nstart, nend - nstart)) == NULL) return 0;
 

//Get device ID key and insert to hash table
  dev_id = makedev(major, minor);
  sprintf(entry.key, "%x", (int)dev_id);
  entry.data = (void *)dev_mat[key_cnt];
//  printf("Insert entry %s:%s\n", entry.key, (char *)entry.data);
  hsearch(entry, ENTER); 
  key_cnt++;
  free(smajor);
  free(sminor);
  return 1; 
}

int main(){

  int fd, retc, rc, line = 0;
  char *buff = (char *)malloc(SIZE+1), *cp, *cpp;
  char *dfname = "/dev/zero";
  char *pfname = "/proc/partitions";
  struct stat dfstat;
  key_cnt = 0;
  ENTRY fentry, *enp;
  fentry.key = (char *)malloc(SIZE * sizeof(char));

  if((retc = stat(dfname, &dfstat)) <0) perror("Fail to stat datafile");
   
  sprintf(fentry.key,"%x", (int) dfstat.st_dev);

  printf("zero entry = %s\n", fentry.key);

  if((fd = open(pfname, O_RDONLY)) < 0)
  {perror("Cannot open /proc/partitions");
   exit(EXIT_FAILURE);
  }

  //Create hash table
  if((retc = hcreate(SIZE)) == 0) 
  {perror("Cannot create hash table");
   exit(EXIT_FAILURE);
  }

  do { retc = read(fd, (void *)buff, (size_t)SIZE); }
  while (retc < 0 && errno == EINTR);

  cp = cpp = buff;
   
  while(retc > 0){
   while(*cp != '\n' && retc > 0) {cp++; retc--;}
   if(retc <= 0) break;
   line++;
   *cp = '\0'; 
   if(line > 2 && strlen(cpp) > 0 ) 
    if((rc = putHtable(cpp, cp)) == 0) 
    {perror("Cannot phrase line");
     exit(EXIT_FAILURE);
    }
     cpp = cp + 1; 
  }

   if((enp = hsearch(fentry, FIND)) == NULL)
     perror("cannot find the partition is the hash table");
   else
     printf("Search success: %s\n", (char *)enp->data);

   free(buff);
   free(fentry.key);

   hdestroy();
   close(fd);
   exit(EXIT_SUCCESS);
}
