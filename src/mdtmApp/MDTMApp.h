/*
 * MDTMApp.h
 * 
 * Created on: May 20, 2014
 * Author: Tan Li
 */

//#define MDTM
#include <stdlib.h>
#include <utime.h>
#include <string.h>
#include <search.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <numa.h>

#ifdef MDTM
#include "mdtm.h"
#include "mdtm/mdtm_tree.h"
#endif

#include "bbcp_Link.h"
#include "bbcp_Node.h"
#include "bbcp_System.h"
#include "bbcp_Emsg.h"
#include "bbcp_Pthread.h"
#include "bbcp_FileSpec.h"
#include "bbcp_Network.h"
#include "bbcp_BuffPool.h"
#include "bbcp_FileSystem.h"
#include "bbcp_Timer.h"

#define REPORT_TIME 2
#define DAEMON_PORT 6031
#define SIZE 512
#define WAIT_TIME 500
#define REQ_PIPE "/tmp/mdtmapp_req"
#define LOG_PATH "/tmp/mdtmapp_log"
#define IF_SEARCH "ip route get %s"
#define REQ_SNK_FFMT "%d %s %d %lld %c %o %lld %s %s %s %lld %s %s %s %s %d %d %d %d %d %d %d %d %d %s %d %lld %c %o %lld %s %s %s %lld\n"
#define REQ_SNK_FMT "%d %s %d %lld %c %o %lld %s %s %s %lld %s %s\n"
#define REQ_SRC_FFMT "%d %s %d %lld %c %o %lld %s %s %s %lld %s %s %d %d %d %d %d %d %d %d %s %d %d %d\n"
#define REQ_SRC_FMT "%d %s %d %lld %c %o %lld %s %s %s %lld\n"

#define BufSzComp(rwb) (rwb = (rwb < bbcp_OS.PageSize ? bbcp_OS.PageSize : rwb/bbcp_OS.PageSize*bbcp_OS.PageSize))

#define ThrCompute(c) ((((double) c * 28.0) / 40.0) + 4.0)


enum EventType {NET_COMP, NET_ERR, STOR_COMP, STOR_ERR, AGENT_OFF};

typedef struct MDTMApp_NetDev{
    struct  in_addr Addr;
    int NetMask;
    char *DevName;
    MDTMApp_NetDev *next;
} MDTMApp_IPDev;

typedef struct MDTMApp_Sess{
    char    *ID;
    char    *user;
    char    *hostname;
    char    *CBhost;
    int     nstrm;
    int     CBport;
    int     file_cnt;
    int     tfile_cnt;
    int     cnt;
    int     RWBsz;
    int     Mode;
    int     Wsize;
    int     Bfact;
    int     BNum;
    int     SNKDevID;
    int     iodepth;
    bbcp_Timer  exe_time;
    bbcp_FileSpec   *srcspec;
    bbcp_FileSpec   *snkspec;
    bbcp_Stream     FStream;
    long long       Options;
    long long       DoneBytes, PDoneBytes, PTime;
    long long       TotalBytes;
    pid_t   Agent_pid;
    MDTMApp_Sess    *next;
}MDTMApp_Session;

typedef struct MDTMApp_Netlink{
    bbcp_Mutex      WorkLock;
    MDTMApp_Task    *Tobj;
    bbcp_Link       *Link;
    pthread_t       tid;
    MDTMApp_Netlink *next;
    int             seqno;
} MDTMApp_Link;

typedef struct MDTMApp_SWorker{
    bbcp_Mutex      WorkLock;
    bbcp_File       *File;
    MDTMApp_Task    *Tobj;
    pthread_t       tid;
    int             Num;
    int             Bfact;
    MDTMApp_SWorker *next;
} MDTMApp_Stor;

typedef struct CompEvent{
    enum EventType Type;
    int         Num;
    int         role; 
    CompEvent   *next;
    bbcp_File   *Fobj;
} MDTMApp_CEvent;
