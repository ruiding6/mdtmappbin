/*
 *  *  *  * MDTAMApp_Network.h
 *   *   *   *
 *    *    *    *  Created on: May 12, 2014
 *     *     *     *      Author: Tan Li
 *      *      *      */

#include <netinet/in.h>

#include "bbcp_Config.h"
#include "bbcp_Network.h"

typedef struct MDTMApp_Sess MDTMApp_Session;
typedef struct MDTMApp_Netlink MDTMApp_Link;
typedef struct MDTMApp_SWorker MDTMApp_Stor;


class MDTMApp_Network{
    public:
        int     Init();
        void    Clean();
        int     GetNetWorker(int TaskCap, MDTMApp_Link* &worker, int role, int bflag, int limit);
        int     RetNetWorker(MDTMApp_Link* &task_link, int num, int role);

        char    Name[10];
        char    IOType[10];
        char    DevType[10];
        int     Cap;
        int     NetThrCnt;
        int     Node;
        struct  in_addr IPAddr;
        MDTMApp_Network *next;

        MDTMApp_Network():Cap(0), NetThrCnt(0), SenderList(0), RecverList(0), FreeSender(0), FreeRecver(0), next(0), SenderLock(0), RecverLock(0), Node(-1) {}
        ~MDTMApp_Network() {Clean();}

    private:
        //Sender pool
        int     FreeSender;
        MDTMApp_Link   *SenderList;
        bbcp_CondVar   *SenderLock;

        //Recver pool
        int     FreeRecver;
        MDTMApp_Link   *RecverList;
        bbcp_CondVar    *RecverLock;
};
