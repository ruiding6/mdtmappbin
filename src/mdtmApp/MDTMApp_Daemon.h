/*
 *  * MDTAMApp_Daemon.h
 *   *
 *    *  Created on: May 8, 2014
 *     *      Author: Tan Li
 *      */

#include "MDTMApp.h"

class MDTMApp_Task;
class MDTMApp_TaskGroup;
class MDTMApp_Network;

class MDTMApp_Daemon{
    public:
        int	    Init(int Dmode, int isDebug);
        int	    GetRequest();
        int     ProgReport();
        int     LinkAccept();
        int     LinkProc(bbcp_Link *link);

#ifdef MDTM
        int    ScheduleThread(pthread_t tid, char *DevN);
#endif 

        int     UpdateTranSess(MDTMApp_Session  *sp, MDTMApp_Task *taskp);
        int     AttachReqPipe(int fd);
        void    Clean();
        void    CancelTasks();
        void    UpdateFileCnt(MDTMApp_Session *sp, int newCnt, long long newData);
        void     _DeleteFileSpec(bbcp_FileSpec *pFSpec, int mode = 0);

        MDTMApp_Daemon():Debug(0), DefNetIF(0), IPTable(0), dev_cnt(0), StorDevGroup(0), NetDevGroup(0), LinkAccTid(0), Session_list_init(0), Session_list_tran(0), mode(0) {
        ExcuPipe[0] = 0;
        ExcuPipe[1] = 0;

#ifdef MDTM
        tree = 0;
#endif
         }
        ~MDTMApp_Daemon() {}

        char    *DefNetIF;
        int     ExcuPipe[2];
        int     Debug;
        MDTMApp_IPDev   *IPTable;
        MDTMApp_Network	*NetDevGroup;
        MDTMApp_TaskGroup	*StorDevGroup;

    private:
        void     _DeactivateTaskList(MDTMApp_Task *const pTList, MDTMApp_Session *pSess);
        inline void _FreeChArray(char* pc) {
            if (pc) {
                free(pc);
            }
        }
        void    _FreeSession(MDTMApp_Session *pSess);

#ifdef MDTM
        int	    GetTopo();
#endif

        int	    DevConfigProc();
        int     Daemonize();
        int     IPTableProc();
        int	    AddDevice(bbcp_Stream *stream);
        void	AddSession(MDTMApp_Session* &head, bbcp_Mutex *list_lock, MDTMApp_Session *session);
        int	    AddFile(bbcp_FileSpec *fp);
        int	    RemoveInit(MDTMApp_Session *sp);
        void	Show(MDTMApp_Session *head, MDTMApp_Session *sp, const char *action);
        bbcp_FileSpec   *LoadFileSpec(bbcp_Stream *req_stream, int seqno);
        int     PostTask(MDTMApp_Session *sp);
        int     FeedBack(MDTMApp_Session *ssp, int DoneFile, long long DoneData);
        

        pthread_t   LinkAccTid, ProgReptTid;
        int         mode;
        int	        dev_cnt;
        bbcp_Stream	req_stream;

#ifdef MDTM
        mdtm_node_t	*tree;
#endif

        bbcp_Mutex	exec_fence;
        MDTMApp_Session	*Session_list_init;
        bbcp_Mutex	slist_init;
        MDTMApp_Session	*Session_list_tran;
        bbcp_Mutex	slist_tran;
};
