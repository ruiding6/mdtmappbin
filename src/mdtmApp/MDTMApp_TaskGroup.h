/*
 * MDTAMApp_TaskGroup.h
 *
 * Created on: Feb 8, 2014
 * Author: Tan Li
 */

#ifndef __MDTMAPP_TASKGROUP_H__
#define __MDTMAPP_TASKGROUP_H__

#include <libaio.h>

#ifdef MDTM
#include "mdtm.h"
#include "mdtm/mdtm_tree.h"
#endif

#include "bbcp_Pthread.h"

#define SIZE 512

typedef struct MDTMApp_Sess MDTMApp_Session;
typedef struct MDTMApp_Netlink MDTMApp_Link;
typedef struct MDTMApp_SWorker MDTMApp_Stor;
typedef struct CompEvent MDTMApp_CEvent;

class MDTMApp_Task;
class MDTMApp_Network;
class bbcp_Link;
class bbcp_Stream;
class bbcp_Node;
class bbcp_File;
class bbcp_FileSpec;
class bbcp_BuffPool;
class bbcp_FileSystem;

typedef struct {
    char	LDName[10];
    char	RDName[10];
    char	IOType[10];
    char	DevType[10];
    int cap;
} mdtmApp_dev_t;

//MDTMApp task group class
class MDTMApp_TaskGroup{

    public:
        int     Init();
        int     DevConfigProc();
        int     SendRequest(int file_cnt);
        void    AddTask(MDTMApp_Task *taskp, int Lflag, int role);
        int     RemoveTask(MDTMApp_Task *taskp, int Lflag, int role);
        void    TaskSort();
        void	Show();
        int	    AttachNet(unsigned long s_addr);
        void	SetThreadNum();
        void	SetNetThrNum(int netstream) {NNet = netstream;}
        int	    GetStorThrNum() {return NReader;}
        int	    GetNetThrNum() {return NNet;}
        int	    PollTask(int role);
        int     PutLink(bbcp_Link *link);
        int     GetStorWorker(int TaskCap, MDTMApp_Stor* &worker, int role);
        int     RetStorWorker(MDTMApp_Stor* &worker, int num, int role);
        int     SharedDaemonProc(int isDebug);
        int     ExcuDaemonProc(int isDebug);        

        MDTMApp_TaskGroup(int key, int flag): StorDevName(0), NetDevName(0), StorType(0), LDevName(0), StorDevID(key), next(0), key_cnt(0), Daemon_flag(flag), SendList(0), SendListSem(0), SendTaskCnt(0), SendTranList(0), STCnt(0), RecvList(0), RecvListSem(0), RecvTaskCnt(0), RecvTranList(0), RTCnt(0), FreeReader(0), ReaderLock(0), ReaderList(0), WriterLock(0), WriterList(0), StorThrCnt(0), RTLock(0), STLock(0), Node(-1) {SendListLock = new bbcp_CondVar(0); RecvListLock = new bbcp_CondVar(0);} 
        ~MDTMApp_TaskGroup() {ClearTask();}

        MDTMApp_TaskGroup	*next;
        int      Daemon_flag;
        int      StorDevID;
        char    *StorDevName;
        char	*NetDevName;
        char	*StorType;
        char	*LDevName;
        int      StorThrCnt;
        int      StorCap;
        int      RTCnt;
        MDTMApp_Task    *RecvTranList;
        bbcp_CondVar	*RTLock;
        int      Node;
        dev_t    Dev_id;
        pthread_t  SendTid, RecvTid;
        MDTMApp_Task	*SendList;

        MDTMApp_Task    *SendTranList;
        bbcp_CondVar    *STLock;
        int STCnt;

    private:
        void    ClearTask();
        int     AddDevice(bbcp_Stream *stream);
        void    Swap(MDTMApp_Task *task1, MDTMApp_Task *task2);
        MDTMApp_Task    *Partition(MDTMApp_Task *low, MDTMApp_Task *high);
        void    QuickSort(MDTMApp_Task *low, MDTMApp_Task *high);
        int     RequestNetThr(int CtlFD, int sug_num, int role);
        int     Daemonize();

        int	key_cnt;
        char	key_mat[SIZE][32];
        mdtmApp_dev_t	mdtmApp_DevList[SIZE];
        int	NReader;
        int	NNet;
        char    *NetType;
        int	NetCap;

        //Send task queue
        bbcp_CondVar	*SendListLock;
        bbcp_Semaphore	SendListSem;
        int SendTaskCnt;


        //Recv task queue
        MDTMApp_Task	*RecvList;
        bbcp_CondVar    *RecvListLock;
        bbcp_Semaphore  RecvListSem;
        int RecvTaskCnt;


        //Reader pool
        int FreeReader;
        bbcp_CondVar    *ReaderLock;
        MDTMApp_Stor    *ReaderList;
        
        //Writer pool
        int FreeWriter;
        bbcp_CondVar    *WriterLock;
        MDTMApp_Stor    *WriterList;
};


class MDTMApp_Task{
    public:
        int     File2Task(int role, bbcp_Node *Node, struct stat *fstat, struct fiemap *filemap, char keyStr[]);
        int     Process();
        int     Request();
        int     Write_Normal();
        int     Write_Direct();
        int     Write_Aio();
        int     Net2Buff(MDTMApp_Link *strms, int sflag, int mode);
        int     Buff2Net(MDTMApp_Link *strms, int mode);
        long long Stats();
        void    PostCEvent(MDTMApp_CEvent *event);
        int     GetNetNode();
        int     SendExt();
        int     FileStat(const char *path, void *fInfo);
        int     ParsePath(bbcp_FileSpec *target, int &newFCnt, long long &newDataSz);

        bbcp_FileSpec	*srcspec, **srcArray;
        bbcp_FileSpec	*firstSrcDSpec, *lastSrcDSpec;
        bbcp_FileSpec	*firstSrcFSpec, *lastSrcFSpec;
        bbcp_FileSpec	*snkspec;
        MDTMApp_Task	*next, *prev;
        bbcp_BuffPool   *BuffPool;
        MDTMApp_Session	*session;
        MDTMApp_TaskGroup   *Group;
        int	cnt, file_cnt, active;
        int dir_cnt;
        int BNum;
        pthread_t ServTid;
        bbcp_FileSystem *fs_obj;
        int NetWCnt;
        long long TotalBytes;

        //link pool for SNK
        bbcp_Mutex      RecvLinkLock;
        bbcp_Semaphore  RecvLinkSem;
        bbcp_Link      *RecvLinkPool;
        bbcp_Node   *Remote;  //control channel

        MDTMApp_Task(bbcp_FileSpec *src_fp, bbcp_FileSpec *snk_fp): srcspec(src_fp), snkspec(snk_fp), firstSrcDSpec(0), lastSrcDSpec(0), firstSrcFSpec(0), lastSrcFSpec(0), next(0), prev(0), active(1), file_cnt(0), session(0), Remote(0), Group(0), RecvLinkSem(0), RecvLinkPool(0), NetWorkers(0), NetWCnt(0), StorWorkers(0), StorWCnt(0), BuffPool(0), NetDev(0), TranTid(0), CompleteSem(0), CEList(0), LastOffset(0), cnt(0), TotalBytes(0), DoneBytes(0), ActiveFile(0), srcArray(0), BNum(0) {}
        MDTMApp_Task(): srcspec(0), snkspec(0), firstSrcDSpec(0), lastSrcDSpec(0), firstSrcFSpec(0), lastSrcFSpec(0), next(0), prev(0), active(1), file_cnt(0), session(0), Remote(0), Group(0), RecvLinkSem(0), RecvLinkPool(0), NetWorkers(0), NetWCnt(0), StorWorkers(0), StorWCnt(0), BuffPool(0), NetDev(0), TranTid(0), CompleteSem(0), CEList(0), LastOffset(0), cnt(0), TotalBytes(0), DoneBytes(0), ActiveFile(0), srcArray(0), BNum(0) {}
        ~MDTMApp_Task() {Clean();}

    private:
        int     ProcessLogin(bbcp_Link *link); 
        int     OpenFile(bbcp_FileSpec *fp, int role);
        int     Finalize(bbcp_FileSpec *fp);
        int     SendTask();
        int     PipelineSend();
        int     StripeSend();
        int     ProcessExit(); 
        int     SendFile(bbcp_FileSpec *fp);
        int     GetLocalIF();
        int     RequestLogin(bbcp_Link *link);
        int     RequestGet(bbcp_FileSpec *fp);  
        int     RequestExit(int retc);
        int     RecvFile(bbcp_FileSpec *fp);
        int     GetLink(bbcp_Link* &link, int NotCtl);
        int     AdjustWS(char *wp, char *bp, int Final, int role);
        int     AdjustRes(char *wp, char *nwp);
        void    Clean();
        int     CEventHandler(int &Rsequ, long long &DDate);
        void    AddOFile(bbcp_File *fopp);
        int     RmOFile(bbcp_File *fopp);
        bbcp_File *GetFileObj(bbcp_FileSpec *fp, int bseq);
        MDTMApp_Link   *NetWorkers;
        MDTMApp_Stor    *StorWorkers;
        int StorWCnt;
        MDTMApp_Network *NetDev;
        pthread_t   TranTid;
        long long   LastOffset, DoneBytes;
        int Cap;
        bbcp_Mutex      AFOLock;
        bbcp_Mutex      **OpenLock;
        bbcp_File       *ActiveFile;

        //Complete Event Queue        
        bbcp_Semaphore  CompleteSem;
        bbcp_Mutex      CompleteLock;
        MDTMApp_CEvent *CEList;
};

#endif
